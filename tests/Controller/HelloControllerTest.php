<?php

namespace App\Tests\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class HelloControllerTest extends WebTestCase {

    public function testIndex(): void
    {
        $client = static::createClient();
        $crawler = $client->request('GET', '/api/v1/hello');

        $this->assertResponseIsSuccessful();

        $this->assertJson('{"Hello":"world !!!"}');
    }

    public function testName(): void
    {
        $client = static::createClient();
        $crawler = $client->request('GET', '/api/v1/hello/doudou');

        $this->assertResponseIsSuccessful();

        $this->assertJson('{"Hello":"doudou !!!"}');
    }

}
