<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

#[Route('/api/v1/hello')]
class HelloController extends AbstractController
{
    #[Route()]
    public function index(): Response
    {
        return $this->json(["Hello" => "world !!!"]);
    }

    #[Route('/{name}')]
    public function name(string $name)
    {
        return $this->json("Hello $name !!!");
    }
}
