# Update the VARIANT arg in docker-compose.yml to pick a PHP version: 7, 7.4, 7.3
ARG VARIANT=7
FROM mcr.microsoft.com/vscode/devcontainers/php:0-${VARIANT}

# Install MariaDB client
RUN apt-get update && export DEBIAN_FRONTEND=noninteractive \
    && apt-get install -y mariadb-client \
    && apt-get clean -y && rm -rf /var/lib/apt/lists/*

# Update args in docker-compose.yaml to set the UID/GID of the "vscode" user.
ARG USER_UID=1000
ARG USER_GID=$USER_UID
RUN if [ "$USER_GID" != "1000" ] || [ "$USER_UID" != "1000" ]; then groupmod --gid $USER_GID vscode && usermod --uid $USER_UID --gid $USER_GID vscode; fi

# Install php-mysql driver
RUN docker-php-ext-install mysqli pdo pdo_mysql

# Install a version of Node.js using nvm for front end dev
ARG INSTALL_NODE="true"
ARG NODE_VERSION="lts/*"
RUN if [ "${INSTALL_NODE}" = "true" ]; then su vscode -c "umask 0002 && . /usr/local/share/nvm/nvm.sh && nvm install ${NODE_VERSION} 2>&1"; fi

# Install PHPCS
RUN composer global require squizlabs/php_codesniffer

# Install PHP extension: zip
RUN apt-get update && export DEBIAN_FRONTEND=noninteractive \
    && apt-get install -y libzip-dev \
    && apt-get clean -y && rm -rf /var/lib/apt/lists/* \
    && docker-php-ext-configure zip \
    && docker-php-ext-install zip

# Install PHP extension: intl
RUN apt-get update && export DEBIAN_FRONTEND=noninteractive \
    && apt-get install -y libicu-dev \
    && apt-get clean -y && rm -rf /var/lib/apt/lists/* \
    && docker-php-ext-configure intl \
    && docker-php-ext-install intl

# Install PHP extension: opcache
RUN docker-php-ext-configure opcache \
    && docker-php-ext-install opcache

# Install Symfony CLI
RUN curl -sS https://get.symfony.com/cli/installer | bash \
    && mv /root/.symfony/bin/symfony /usr/local/bin/symfony \
    && rmdir /root/.symfony/bin /root/.symfony \
    && /usr/local/bin/symfony self-update --no-interaction

# Uncomment this section to install additional OS packages.
RUN apt-get update && export DEBIAN_FRONTEND=noninteractive \
    && apt-get -y install --no-install-recommends \
    bash-completion \
    git \
    jq \
    lsb-release \
    unzip \
    vim

ENV PATH="${PATH}:~/.composer/vendor/bin"
